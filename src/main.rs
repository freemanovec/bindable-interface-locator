use clap::Clap;
use ipnetwork::IpNetwork;
use std::{
    process,
    net::TcpListener
};

#[derive(Clap)]
#[clap(version = "0.1", author = "Jakub Rohla <freemanovec@protonmail.com>")]
struct Opts {
    #[clap(short = "a", long = "address", help = "Address or range to scan in CIDR notation")]
    address: String,
    #[clap(short = "p", long = "port", help = "Port to try and bind")]
    port: u16,
    #[clap(short = "v", long = "verbose")]
    verbose: bool,
    #[clap(short = "s", long = "short", help = "Only output addresses to stdout")]
    short: bool
}

macro_rules! bail {
    () => (process::exit(1));
    ($($arg:tt)*) => ({
        eprintln!("{}", &format!("{}\n", format_args!($($arg)*)));
        process::exit(1);
    })
}

fn main() {
    let Opts { address, port, verbose, short } = Opts::parse();
    let range = address.parse::<IpNetwork>();
    if range.is_err() {
        bail!("Invalid IP specified: {}", &range.unwrap_err());
    }
    let range = range.unwrap();
    for addr in &range {
        if verbose { eprintln!("Trying address {}", &addr) }
        let addrstr = format!("{}:{}", &addr, port);
        let listener = TcpListener::bind(addrstr);
        if listener.is_ok() {
            if short {
                println!("{}", &addr);
            } else {
                println!("Successfully bound to {}", &addr);
            }
        } else{
            if verbose { eprintln!("Unable to bind to {}", &addr); }
        }
    }
}
